# Beth

## Rental management system


### Problem: Connecting tenants & landlords

**“How fast can you get a house, room for renting in Uganda?”**

Have taken time analyzing the renting business in Uganda: its processes and procedures; it’s really painful, time consuming, costly, a lot of movement in acquiring a rental, challenge of keeping records and obtaining payment from tenants. Have been able to get the landlord and tenant's view: from my neighbor and personal experience respectively.

My neighbor after constructing her two rentals couldn’t get clients for months then a year, she came to me: ‘I couldn’t help’. She got a relative who rented for some time. On return for holidays I wasn’t surprised that she turned the rentals into a kindergarten.

 Living in a residential area ‘Wakiso’ : many rentals have been set up but I have observed over years nice houses going without clients for long periods. The landlords make expensive banners and some paint  “house for rent” on their building I always pass by day in day out: no tenants: There is a big gap between the tenants and the landlords.

Having been in renting in four places securing a rental has not been easy each time. My conclusion: due to the gap that exists between the tenants and landlords, clients do not get the best for their money in most cases; this is majorly because there is no single place to access rentals in a given area. An area can have three brokers whose services are not only costly, involves a lot of movement and don’t have realtime details of the state of the houses.

Am looking forward to providing a platform that will bridge the gap that exist between the landlords and tenants. It will be a real time mobile application. Focused on : **“access and management”** .

### Management
Platform will enable landlords: (owners of houses, apartments, hotels, acades) to advertise by uploading images, videos, details of their rentals, location of rental on a google map and the terms.
The application will store details on when tenant acquires the property and duration. Application will send payment reminders and notify tenant to renew contract ahead of expiry and track tenant payments.

### Access
Application will display to tenants available property  in a specified location, the cost and terms. So clients can compare and get the best. For certified landlords tenants will be able to pay online. The application will provide renting articles: advice, tenants will be able to give feedback and rate landlords .

The solution will require landlords and tenants to have smartphones and access to internet  to run the application.

## Impact on economy:

    -  Competition in the renting business will intensify and it will force landlords to improve their infrastructure and services to cope up thereby boosting economy.
    -  Many be motivated and encouraged to stay in the business as market will be available.
    -  It will reduce on wastage of resources: money and time in acquiring rentals and also time in which rentals are vacant since application is real time: once a tenant vacates  the rental is marked available.

### Advice / feedback highly welcome
 **fitzerkisakyamukama95@gmail.com**
